DESCRIPTON = "Makes emgucv available for yocto"
LICENSE = "GPL-3.0+"
LIC_FILES_CHKSUM = "file://../git/LICENSE;md5=f6e87a6211087d6a77a64ffe96caf61d"
PR = "r0"

RDEPENDS_${PN} += "bash"

SRCREV_emgucv = "a61a1bfe67a619e561b8831b0f53c746ca404a0c"
SRCREV_FORMAT = "emgucv"
SRC_URI = "git://github.com/emgucv/emgucv.git;name=emgucv"


S = "${WORKDIR}/git"

inherit pkgconfig cmake

do_configure() {
	cd ${S}
	${S}/platforms/raspberry_pi_os/apt_install_dependency
	${S}/platforms/raspberry_pi_os/cmake_configure
	cmake .
	cmake --build ./ 
}

do_install() {
	install -d ${D}${bindir}/emgucv
}