#! /bin/bash
interface=$1
event=$2

# This event only occurs when the eth0 interface is brought up
# The scheel script is automagically found by NetworkManager and ran
# See https://developer-old.gnome.org/NetworkManager/stable/NetworkManager.html section Dispatcher scripts
if [[ $interface != "eth0" ]] || [[ $event != "up" ]]
then
        exit 0
fi

# Get GPIO pin level of Z PCB. This selects the proper function of the DHCP server
GPIOOUTPUT=$(eval "gpioget gpiochip0 6")
echo "GPIO Pin Level for Instrument Secondary Selection:"
echo $GPIOOUTPUT

if [ $GPIOOUTPUT -eq 0 ]; then
        echo "Starting Internal Instrument DHCP Server..."
		
		# Check integrity of the dhcp config here
		if [ -r "/etc/kea/kea-dhcp4.conf" ]; then
			echo "Integrity of the dhcp config is consistent"
			sudo keactrl start
		else
			echo "The file '/etc/kea/kea-dhcp4.conf' is broken or unreadable. Using backup file 'etc/kea/kea-dhcp4_backup.conf' instead."
			sudo cp "/etc/kea/kea-dhcp4_backup.conf" "/etc/kea/kea-dhcp4.conf"
			sudo keactrl start
		fi
else
        echo "Stopping Internal DHCP Server (If On)..."
        sudo keactrl stop
fi

# Turn on IP Forwarding (Critical for NAT Usage)
echo 1 > /proc/sys/net/ipv4/ip_forward

# Restore iptables firewall rules. By default iptables does not restore automagically
iptables-restore < /etc/iptables/iptables.rules
