#! /bin/bash

#get primary/secondary setting from switch
GPIOOUTPUT=$(eval "gpioget gpiochip0 6")
echo "GPIO Pin Level for Instrument Secondary Selection:"
echo $GPIOOUTPUT

if [ $GPIOOUTPUT -eq 0 ]; then
	echo "Enabling Internal Instrument DHCP Server..."
	
	#Configure first interface to use static IP instead of DHCP
	sudo cp -f /etc/network/configs/interfaces_primary /etc/network/interfaces
else
	echo "Disabling Internal DHCP Server (If On)..."
	
	#Configure first interface to use DHCP instead of static IP
	sudo cp -f /etc/network/configs/interfaces_secondary /etc/network/interfaces
	sudo keactrl stop
fi

if [ -f /torrent_ps/init_states/folderStructureinitialized_A.txt ]; then
	echo "Persistant Folder Structure already created."
else
	echo "starting Persistant Folder Structure Creation."
	mkdir /torrent_ps/init_states
	mkdir /torrent_ps/systemupdates
	mkdir /torrent_ps/logs
	mkdir /torrent_ps/backups/instrumentconfig
	mkdir /torrent_ps/backups/db
	mkdir /torrent_ps/backups/db/system
	mkdir /torrent_ps/backups/db/instrument
	chown postgres:postgres /torrent_ps/backups/db
	chown postgres:postgres /torrent_ps/backups/db/system
	chown postgres:postgres /torrent_ps/backups/db/instrument
	touch /torrent_ps/init_states/folderStructureinitialized_A.txt
fi

if [ -f /torrent/init_states/software_unpacked.txt ]; then
	echo "Main Software unpacking already done."
else
	echo "starting Main Software unpacking."
	cd /torrent/linux
	unzip compressed.zip
	rm compressedsoftware.zip
	chmod +x Tempest.RealInstrument
	cd /torrent/reporting
	unzip compressedreporting.zip
	rm compressed.zip
	chmod +x Tempest.WebReporting
	touch /torrent/init_states/software_unpacked.txt
fi	

# if [ -f /torrent/init_states/config_files_restored.txt ]; then
# 	echo "Config Files are restored."
# else
# 	echo "restoring config files."
# 	cp /torrent_ps/backups/instrumentconfig/*.* /torrent/instrumentconfig/
# 	touch /torrent/init_states/config_files_restored.txt
# fi	

if [ -f /torrent/init_states/dbinitialized.txt ]; then
	echo "db already initialized"
else
	echo "starting db initialization"	
	systemctl disable realinstrument.service
    systemctl stop realinstrument.service
	chown postgres:postgres /torrent/db
	sudo chpasswd <<<"postgres:postgres"
	cd /usr/bin
	su postgres -c 'initdb -D /torrent/db'
	#setup postgres
	mkdir /lib/systemd/system/postgresql.service.d
	printf "[Unit]\nAfter=tempeststartup.service\n[Service]\nEnvironment=PGDATA=/torrent/db" >> /lib/systemd/system/postgresql.service.d/override.conf
	systemctl daemon-reload
	systemctl reload-or-restart postgresql
	#restore databases
    cd /torrent_ps/backups/db/instrument
	backupname=$(ls -t * | head -1)
	if [ "$backupname" ]; then		
	    su postgres -c 'createdb InstrumentDb'
        su postgres -c 'psql -U postgres -d InstrumentDb < $0' $backupname
	fi
	cd /torrent_ps/backups/db/system
	backupname=$(ls -t * | head -1)
	if [ "$backupname" ]; then		
	     su postgres -c 'createdb tempestDb'
         su postgres -c 'psql -U postgres -d tempestDb < $0' $backupname
	fi
	systemctl enable postgresql
    systemctl enable realinstrument.service
    if [ $GPIOOUTPUT -eq 0 ]; then # when not a secondairy instrument
    	systemctl enable torrentreporting.service
    fi
    touch /torrent/init_states/dbinitialized.txt
    reboot now
    exit 0	
fi

if [ -f /torrent/init_states/enable_services.txt ]; then
	echo "Services are already enabled."
else
	echo "Enabling services."
    systemctl enable systemd-resolved.service
	touch /torrent/init_states/enable_services.txt
fi

#release uart4 from BT by disabling buffer
gpioset gpiochip1 9=1

# Disable dumb automatic power configuration for USB Hub on driverboard
echo 'on' > '/sys/bus/usb/devices/4-1/power/control'
echo 'on' > '/sys/bus/usb/devices/3-1/power/control'
echo 'on' > '/sys/bus/usb/devices/usb1/power/control'
echo 'on' > '/sys/bus/usb/devices/usb2/power/control'
echo 'on' > '/sys/bus/usb/devices/usb3/power/control'
echo 'on' > '/sys/bus/usb/devices/usb4/power/control'
echo 'on' > '/sys/bus/i2c/devices/i2c-0/device/power/control'
echo 'on' > '/sys/bus/i2c/devices/i2c-1/device/power/control'
echo 'on' > '/sys/bus/i2c/devices/i2c-2/device/power/control'
echo 'on' > '/sys/bus/i2c/devices/i2c-3/device/power/control'
