DESCRIPTON = "Startup script for Torrent"
LICENSE = "MIT"
# ${COREBASE} points to the poky repo for some reason 
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE.MIT;md5=030cb33d2af49ccebca74d0588b84a21"

# Recipe revision - don't forget to 'bump' when a new revision is created !
PR = "r17"

FILES_${PN} += "/torrent/*"
FILES_${PN} += "/torrent/android/*"
FILES_${PN} += "/torrent/linux/*"
FILES_${PN} += "/torrent/ui/*"
FILES_${PN} += "/torrent/firmware/*"
FILES_${PN} += "/torrent/scripts/*"
FILES_${PN} += "/torrent_ps/"

#
# Systemd services
#
inherit systemd
SYSTEMD_SERVICE_${PN} = "tempeststartup.service realinstrument.service torrentui.service"

# Runtime dependencies
#
# Add a line similar to the following to ensure any packages needed for the scripts to run are installed in the image.
#
RDEPENDS_${PN} += "bash"

# These lines prevent the exception "already stripped" which is thrown for already compiled files
INSANE_SKIP_${PN} = "ldflags"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

# SRC_URI specifies the files that are to be used as the scripts.
#
# Any valid src_uri format can be used - this example assumes the
# scripts are stored in the 'files' directory below the one in
# which this file is located.
#
SRC_URI = "file://tempeststartup.sh \
		   file://dhcpserverscript.sh \
           file://realinstrument.service \
           file://torrentui.service \
           file://tempeststartup.service \
           file://linuxapp/compressedsoftware.zip;unpack=0 \
           file://uiservice/compressedui.zip;unpack=0 \
           file://androidapp/browser.apk;unpack=0 \
           file://firmware/ \
		   file://interfaces_primary \
		   file://interfaces_secondary \
		   file://NetworkManager.conf \
		   file://99-usb-serial.rules \
		   file://iptables.rules"

# This function is responsible for:
#  1) Installing the scripts in to the image;
#  2) Creating the desired runlevel links to the scripts.
#
do_install() {

    # Create directories:
    #   ${D}${sysconfdir}/init.d - will hold the scripts
    #   ${D}${sysconfdir}/rcS.d  - will contain a link to the script that runs at startup
    #   ${D}${sysconfdir}/rc3.d  - will contain a link to the script that runs at runlevel=3
    #
    # ${D} is effectively the root directory of the target system.
    # ${D}${sysconfdir} is where system configuration files are to be stored (e.g. /etc).
    #
    install -d ${D}${sysconfdir}/systemd/system
    install -d ${D}/torrent/android
    install -d ${D}/torrent/linux
    install -d ${D}/torrent/ui
    install -d ${D}/torrent/firmware
    install -d ${D}/torrent/init_states
    install -d ${D}/torrent/db
    install -d ${D}/torrent/instrumentconfig
    install -d ${D}/torrent/scripts
    install -d ${D}/torrent_ps
    install -d ${D}${sysconfdir}/network
	install -d ${D}${sysconfdir}/network/configs
	install -d ${D}${sysconfdir}/NetworkManager
	install -d ${D}${sysconfdir}/NetworkManager/dispatcher.d
	install -d ${D}${sysconfdir}/udev/rules.d
	install -d ${D}${sysconfdir}/etc/iptables

    #
    # Install files in to the image
    #
    # The files fetched via SRC_URI (above) will be in ${WORKDIR}.
    #
   install -m 0755 ${WORKDIR}/tempeststartup.sh  ${D}/torrent/scripts/
   install -m 0644 ${WORKDIR}/tempeststartup.service ${D}${sysconfdir}/systemd/system
   install -m 0644 ${WORKDIR}/realinstrument.service ${D}${sysconfdir}/systemd/system
   install -m 0644 ${WORKDIR}/torrentui.service ${D}${sysconfdir}/systemd/system
   install -m 0644 ${WORKDIR}/linuxapp/compressedsoftware.zip ${D}/torrent/linux/
   install -m 0644 ${WORKDIR}/uiservice/compressedui.zip ${D}/torrent/ui/
   install -m 0644 ${WORKDIR}/androidapp/browser.apk ${D}/torrent/android/   
   for file in ${WORKDIR}/firmware/*.hex;do
    install -m 0755 "$file" ${D}/torrent/firmware/
   done
	install -m 0755 ${WORKDIR}/interfaces_primary ${D}${sysconfdir}/network/configs/
	install -m 0755 ${WORKDIR}/interfaces_secondary ${D}${sysconfdir}/network/configs/
	install -m 0755 ${WORKDIR}/dhcpserverscript.sh ${D}${sysconfdir}/NetworkManager/dispatcher.d
	install -m 0755 ${WORKDIR}/NetworkManager.conf ${D}${sysconfdir}/NetworkManager/
	install -m 0755 ${WORKDIR}/iptables.rules ${D}${sysconfdir}/etc/iptables/
	install -m 0755 ${WORKDIR}/99-usb-serial.rules ${D}${sysconfdir}/udev/rules.d/
}

