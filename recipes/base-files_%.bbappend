do_install_append() {
#
# Append fstab to mount internal USB drive
#  
cat >> ${D}${sysconfdir}/fstab <<EOF

# Mount internal USB drive
/dev/disk/by-path/platform-xhci-hcd.1.auto-usb-0:1.3:1.0-scsi-0:0:0:0-part1 /torrent_ps ext4 nosuid,nodev,nofail 0 0
EOF

}