DESCRIPTION = "system-config-printer - tool for handling printers"
HOMEPAGE = "https://github.com/OpenPrinting/system-config-printer"
LICENSE = "GPLv2+"

SRC_URI = "git://github.com/OpenPrinting/system-config-printer.git;protocol=https;branch=master"

SRCREV = "2fa211c837e7488c455f5f72f6d30ad6029cb7fd"
S = "${WORKDIR}/git"

LIC_FILES_CHKSUM = "file://${S}/COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

DEPENDS += "autoconf-archive-native desktop-file-utils-native cups gettext glib-2.0 libusb python3 systemd"
			
RDEPENDS:${PN} += " python3-core python3-pprint libnotify python3-dbus python3-pycairo python3-pycups python3-pygobject python3-syslog "

EXTRA_OECONF += "--with-udev-rules --without-xmlto"
				 
do_configure() {
    ${S}/bootstrap || bbnote "${PN} failed to bootstrap"
    oe_runconf
}

# python cups-helpers
FILES:${PN} += "${PYTHON_SITEPACKAGES_DIR}/*"
# dbus-1 and metainfo
FILES:${PN} += "${datadir}/*"
# configure-printers@.service
FILES:${PN} += "${systemd_system_unitdir}/*"