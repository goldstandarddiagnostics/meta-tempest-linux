DESCRIPTION = "IPP-over-USB deamon for CUPS"
HOMEPAGE = "https://github.com/OpenPrinting/ipp-usb"
LICENSE = "BSD-2-Clause"
SRC_URI =  "file://ipp-usb \
            file://LICENSE \
            file://ipp-usb.conf \
            file://71-ipp-usb.rules \
            file://ipp-usb.service \
            file://HP.conf \
            file://blacklist.conf \
            file://default.conf \
            "

LIC_FILES_CHKSUM = "file://LICENSE;md5=35b61b5975388824d233d3f4ee283ad0"

DEPENDS += "cups"

# Recipe revision - don't forget to 'bump' when a new revision is created !
PR = "r3"

# Runtime dependencies
#
# Add a line similar to the following to ensure any packages needed for the scripts to run are installed in the image.
#
RDEPENDS_${PN} += "bash"

# These lines prevent the exception "already stripped" which is thrown for already compiled files
INSANE_SKIP_${PN} = "ldflags"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

S = "${WORKDIR}"

FILES_${PN} += "/usr/lib/systemd/*"
FILES_${PN} += "/usr/lib/systemd/system/*"

do_install() {
  #create folders
  install -d ${D}${sbindir}
  install -d ${D}${sysconfdir}/ipp-
  install -d ${D}${libdir}/udev/rules.d
  install -d ${D}${libdir}/systemd/system
  install -d ${D}${datadir}/ipp-usb/quirks
  #copy files
  install -m 0755 ${WORKDIR}/ipp-usb  ${D}${sbindir}/
  install -m 644 ${WORKDIR}/ipp-usb.conf ${D}${sysconfdir}/ipp-usb 
  install -m 644 ${WORKDIR}/71-ipp-usb.rules ${D}${libdir}/udev/rules.d
  install -m 644 ${WORKDIR}/ipp-usb.service ${D}${libdir}/systemd/system
  install -m 644 ${WORKDIR}/HP.conf ${D}${datadir}/ipp-usb/quirks
  install -m 644 ${WORKDIR}/blacklist.conf ${D}${datadir}/ipp-usb/quirks
  install -m 644 ${WORKDIR}/default.conf ${D}${datadir}/ipp-usb/quirks
}