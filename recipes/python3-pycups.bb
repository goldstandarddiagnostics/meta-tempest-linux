DESCRIPTION = "pycups - CUPS bindings for Python"
HOMEPAGE = "https://github.com/OpenPrinting/pycups"
LICENSE = "GPLv2+"
SRC_URI = "git://github.com/OpenPrinting/pycups.git;protocol=https;branch=master"

SRCREV = "8cbf6d40a0132764ad51e7416aa7034966875091"
S = "${WORKDIR}/git"

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"
DEPENDS += "cups"

inherit pypi setuptools3