# Kea append recipe for overwriting configuration files in default kea directories
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://keactrl.conf \
			file://kea-dhcp4.conf \
			file://kea-dhcp4_backup.conf"

do_install_append() {

	# Install Directory
	install -d ${D}${sysconfdir}/kea

	# Install Custom Config Files
   	install -m 0755 ${WORKDIR}/keactrl.conf ${D}${sysconfdir}/kea/
	install -m 0755 ${WORKDIR}/kea-dhcp4.conf ${D}${sysconfdir}/kea/
}