FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "\
	file://bootstrap.min.torrent.css \
	file://index.html \
"

do_install_append () {
	install -m 644 ${WORKDIR}/bootstrap.min.torrent.css ${D}/www/css/bootstrap.min.css
	install -m 644 ${WORKDIR}/index.html ${D}/www/
}