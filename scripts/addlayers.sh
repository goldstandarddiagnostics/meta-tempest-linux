echo " " >> build_xwayland/conf/bblayers.conf
echo "# Layers for Torrent" >> build_xwayland/conf/bblayers.conf
echo "BBLAYERS += \"\${BSPDIR}/sources/meta-openembedded/meta-perl\"" >> build_xwayland/conf/bblayers.conf
echo "BBLAYERS += \"\${BSPDIR}/sources/meta-java\"" >> build_xwayland/conf/bblayers.conf
echo "BBLAYERS += \"\${BSPDIR}/sources/meta-tempest-linux\"" >> build_xwayland/conf/bblayers.conf
echo "BBLAYERS += \"\${BSPDIR}/sources/meta-mono\"" >> build_xwayland/conf/bblayers.conf
echo "BBLAYERS += \"\${BSPDIR}/sources/meta-security\"" >> build_xwayland/conf/bblayers.conf
echo "BUILD_CFLAGS_append = \" -Wno-error=format-overflow\"" >> build_xwayland/conf/bblayers.conf