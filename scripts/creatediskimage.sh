#!/bin/sh
set -e
echo "Removing old archives and image files..."
rm -f /p/a/t/h imx8mp-var-dart-extended-sd.img.xz
rm -f /p/a/t/h imx8mp-var-dart-extended-sd.img
echo "Creating empty image file..."
dd if=/dev/zero of=imx8mp-var-dart-extended-sd.img bs=1M count=6720
echo "Creating loopback device..."
loopdev=$(losetup -Pf --show imx8mp-var-dart-extended-sd.img)
echo "Created Loopback device is $loopdev"
echo "Invoking Variscite Script..."
MACHINE=imx8mp-var-dart sources/meta-variscite-imx/scripts/var_mk_yocto_sdcard/var-create-yocto-sdcard.sh -r build_xwayland/tmp/deploy/images/imx8mp-var-dart/var-image-swupdate-imx8mp-var-dart "$loopdev"
echo "Removing loopback device..."
losetup -d "$loopdev"
echo "Compressing image file..."
7z a -mx9 -txz  imx8mp-var-dart-extended-sd.img.xz imx8mp-var-dart-extended-sd.img
echo "Done..."

